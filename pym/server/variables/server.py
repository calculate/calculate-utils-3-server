# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.cl_lang import setLocalTranslate
from calculate.lib.datavars import ReadonlyVariable, Variable, VariableError

_ = lambda x: x
setLocalTranslate('cl_server3', sys.modules[__name__])


class VariableClServerName(Variable):
    """
    Название настраиваемого сервиса
    """
    value = ""
    untrusted = True

    def check(self, value):
        if value == 'unix' and self.Get('sr_ldap_set') != 'on':
            raise VariableError(
                _("To manage Unix accounts, a configured LDAP server "
                  "is required"))


class VariableClServerEnvPath(Variable):
    """
    Путь до файла хранящий учётные записи сервера
    """
    value = "/var/lib/calculate/calculate-server/ldap.env"


class VariableSrLdapSet(Variable):
    """
    Настроен ли LDAP
    """
    type = "bool"
    value = "off"

    def init(self):
        self.label = _("LDAP server configured")


class VariableSrUnixSet(Variable):
    """
    Настроен ли UNIX
    """
    type = "bool"

    value = "off"

    def init(self):
        self.label = _("Unix server configured")


class VariableSrSambaSet(Variable):
    """
    Настроена ли Samba
    """
    type = "bool"
    value = "off"

    def init(self):
        self.label = _("Samba server configured")
